import syslog, subprocess
import requests
import xml.etree.ElementTree as ET


def get_token():
    r = requests.get('http://192.168.8.1/api/webserver/SesTokInfo')
    root = ET.fromstring(r.content)
    return root.find('TokInfo').text, root.find('SesInfo').text


def get_last_sms(token,session):
    headers = {
        "__RequestVerificationToken": token,
        "Cookie":session,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "X-Requested-With": "XMLHttpRequest" 
    }
    data = '<?xml version = "1.0" encoding = "UTF-8"?>\n<request><PageIndex>1</PageIndex><ReadCount>1</ReadCount><BoxType>1</BoxType><SortType>0</SortType><Ascending>0</Ascending><UnreadPreferred>1</UnreadPreferred></request>\n'

    r = requests.post( "http://192.168.8.1/api/sms/sms-list", data=data, headers=headers )

    return r

def parse_sms(sms):
    root = ET.fromstring(sms)
    message = root.find('Messages/Message')
    phone = message.find("Phone").text
    content = message.find("Content").text
    whenreceived = message.find("Date").text
    index = message.find("Index").text
    return index,phone,content,whenreceived

def process_action(action):
    if (action.strip().upper() == 'REBOOT'):
        syslog.syslog('SMS:REBOOT action triggered')
        command = 'systemctl reboot'
        process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
        output = process.communicate()[0]
        return True

    return False

def delete_sms(token,session,index):
    headers = {
        "__RequestVerificationToken": token,
        "Cookie":session,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "X-Requested-With": "XMLHttpRequest" 
    }
    data = '<?xml version = "1.0" encoding = "UTF-8"?>\n<request><Index>%s</Index></request>\n'%index
    r = requests.post( "http://192.168.8.1/api/sms/delete-sms", data=data, headers=headers )
    return r

try:
    token,session = get_token()
    r_sms = get_last_sms(token,session)
    index,phone,content,when = parse_sms(r_sms.content)
    token,session = get_token()
    r = delete_sms(token,session,index)
    syslog.syslog(f"SMS:RECEIVED|{phone}|{when}|{content}")
    process_action(content)
except:
    None
    # This is ugly but does the job for now ;-P



