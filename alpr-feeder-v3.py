import os, time
import os.path
from datetime import datetime, timedelta
import json
import logging
import psutil, shutil
import requests
import greenstalk
#from openalpr import Alpr

DIR = "/home/jnano2/egm_hik_02/cambuffer/"
CACHE_DIR = "/home/jnano2/cache"
BS_HOST = "127.0.0.1"
BS_PORT = 11300
BS_TUBE = "alpr"

copy_image_start = datetime(2020,10,13,0,0,0)
copy_image_end = datetime(2021,12,31,0,0,0)

# Setting up logging
log_handler = logging.StreamHandler()
log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_handler.setFormatter(log_formatter)
root_logger = logging.getLogger()
root_logger.addHandler(log_handler)
root_logger.setLevel(logging.DEBUG)
log = logging.getLogger(__name__)
log.info("Logging setup done")

#alpr = Alpr("eu", "/home/egm/openalpr/openalpr.config", "/home/egm/openalpr/runtime_data")


def timestamp(filename):
    filename = os.path.basename(filename)
    spltmp = filename.split("_")
    if len(spltmp)<3:
        return datetime(2020,1,1,0,0,0)
    ts = datetime.strptime(spltmp[2],"%Y%m%d%H%M%S%f")
    #print(ts)
    return ts


class PreSelector:
    def __init__(self, thr_dt, thr_score):
        self.thr_dt = timedelta(seconds=thr_dt)
        self.thr_score = thr_score
        self.inhibited_until = datetime(2020,1,1,0,0,0)
        self.curtime = self.inhibited_until
        self.dt = "-"

    def inhibited(self):
        return self.curtime < self.inhibited_until
    
    def do_inhibit(self):
        log.info("presel goes into INHIBITED")
        self.inhibited_until = self.curtime + self.thr_dt
        
    def preselect(self,filename):
        newtime = timestamp(filename)
        
        self.dt = newtime - self.curtime
        self.curtime = newtime
        if self.inhibited():
            #self.do_inhibit()
            return False
        else:
            return True
        
    def notify_results(self,results):
        log.debug(results)
        min_score = 1.0
        for r in results:
            min_score = min(r["score"], min_score)
        if (min_score > self.thr_score) :
            self.do_inhibit()


def get_ftp_files():
    files = []
    for p in psutil.process_iter():
        if (p.name() == 'vsftpd'):
            files.extend([i.path for i in p.open_files()])
    return files

def get_files_to_process():
    ready_files = []
    ftp_open_files = get_ftp_files()
    for f in os.listdir(DIR):
        f = os.path.join(DIR,f)
        isopen = f in ftp_open_files
        if (not isopen):
            ready_files.append(f)
    ready_files.sort()
    return ready_files

def get_alpr_results(image_path):
    regions = ['fr',]
    config = {}
    data = dict(regions=regions, config=json.dumps(config))
    with open(image_path,'rb') as fp:
        response = requests.post("http://localhost:8080/v1/plate-reader/",
                      files = dict(upload=fp),
                      data =data
                      )
        if (response.status_code < 200 or response.status_code > 300):
            log.error(f"request error {response.status_code} : {response.text}")
            return None
        else :
            log.info(f"response OK")
            return response.text

def finalize_file_processing(image_path):
    os.remove(image_path)
    #shutil.move(image_path, CACHE_DIR)

def preprocessing(image_path):
    try:
        #return alpr.recognize_file(image_path)
        now = datetime.now()
        do_copy = (now > copy_image_start) and (now < copy_image_end)
        if do_copy :
            fname = os.path.split(image_path)[1]
            dest = os.path.join(CACHE_DIR,fname)
            os.link(image_path,dest)
            log.info('file linked to %s'%dest)
        stats = os.stat(image_path)
        return {"file_size":stats.st_size}
    except:
        return ""

def main():
    bs = greenstalk.Client(host=BS_HOST, port=BS_PORT, use=BS_TUBE)
    preselector = PreSelector(5.0, 0.88)
    while(True):
        time.sleep(1.0)
        files = get_files_to_process()
        for f in files:
            log.info(f"START : {f}")
            try:
                #pre = preprocessing(f)
                proc = get_alpr_results(f)
                jproc=json.loads(proc)
                preselector.notify_results(jproc['results'])
                #jproc['preprocessing']=pre
                results = json.dumps(jproc)
                bs.put(results)
            finally:
                finalize_file_processing(f)
                log.info(f"END : {f}")
    
if __name__ == '__main__':
#    print("\n".join(get_ftp_files()))
    log.info("Starting ALPR Feeder - V3.0")
    time.sleep(1.0)
    main()
    
