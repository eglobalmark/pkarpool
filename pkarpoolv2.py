# Pkarpool
# Carpooling parking monitoring

# Setup:
# OpenALPR installed
# alprd (daemon) runnning and feeding beanstalk
# Beanstalk listening on localhost at port 11300
# alpr events pushed into "aplrd" tube

import argparse
import json, time
import os, traceback
import logging
import greenstalk
import mqtt

from PIL import Image

from types import SimpleNamespace

VERSION = "0.1.0"

BS_HOST = "127.0.0.1"
BS_PORT = 11300
ALPRD_TUBE = "alprd"

mqtt_config = SimpleNamespace()
mqtt_config.server = "lora.smart-territory.eu"
mqtt_config.port = 8883
mqtt_config.secretspath =  "./secret"
mqtt_config.cafile = "ca.crt"
mqtt_config.keyfile = "gorazi.key"
mqtt_config.crtfile = "gorazi.crt"
mqtt_config.topic_detection  = "fed4iot/device/SC01/detection"
mqtt_config.topic_service  = "fed4iot/device/SC01/service"

watchdog_max_failed_attempt = 2

# Setting up logging
log_handler = logging.StreamHandler()
log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_handler.setFormatter(log_formatter)
root_logger = logging.getLogger()
root_logger.addHandler(log_handler)
root_logger.setLevel(logging.DEBUG)
log = logging.getLogger(__name__)
log.info("Logging setup done")



def process_one_alprd_detection():
    job = qmgt.reserve()
    try:
        job_data = json.loads(job.body)
        log.debug(f"detection:{job_data['uuid']}")
        pubret = broker_mqtt.publish(mqtt_config.topic_detection,job.body,qos=1)
        log.debug(f"publish requested : {pubret.rc} {pubret.mid} {pubret.is_published()}")
        pubret.wait_for_publish()
        log.debug(f"publish done : {pubret.rc} {pubret.mid} {pubret.is_published()}")
        img = Image.open(os.path.join(args.imgdir,job_data['uuid']+'.jpg'))
        for plate in job_data["results"]:
            plate_id = plate['plate']
            log.debug(f"plate:{plate_id}")
            c = plate['coordinates']
            xmin = min((i['x'] for i in c))
            xmax = max((i['x'] for i in c))
            ymin = min((i['y'] for i in c))
            ymax = max((i['y'] for i in c))
            region = img.crop( (xmin, ymin, xmax, ymax) )
            region.save(os.path.join(args.outdir,job_data['uuid']+f"-{plate_id}"+'.jpg'))
            with open(os.path.join(args.outdir,job_data['uuid']+f"-{plate_id}"+'.txt'),"w") as f:
                f.write(json.dumps(plate,indent=2))
        log.debug(f"{img.format} / {img.size}")
    except Exception:
        traceback.print_exc()
        raise       
    finally:
        log.debug(f"deleting job {job.id}")
        qmgt.delete(job.id)

def do_nothing():
    log.debug("MQTT reported connection is done")

def loop(mqttbroker):
    log.info("starting loop")
    while(True):
        process_one_alprd_detection()

is_mqtt_connected=False
def switch_connected_state(mqttbroker):
    global is_mqtt_connected
    log.debug("switching mqtt connected state")
    is_mqtt_connected=True

    
    
if __name__ == "__main__":
    log.info("Starting pkarpool")
    log.info(f"Version {VERSION}")
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-i", "--imgdir", help="extracted images directory", type=str)
    argparser.add_argument("-o", "--outdir", help="output directory", type=str)
    args = argparser.parse_args()

    log.info("Getting images in %s"%args.imgdir)
    
    log.info("Setting up beanstalkd connexion")
    qmgt = greenstalk.Client(host=BS_HOST, port=BS_PORT)
    qmgt.watch(ALPRD_TUBE)
    
    log.info("Setting up MQTT connexion")
    secrets_path = mqtt_config.secretspath
    broker_mqtt = mqtt.MqttBroker(mqtt_config.server,mqtt_config.port, switch_connected_state)
    broker_mqtt.ca_file = os.path.join(secrets_path,mqtt_config.cafile)
    broker_mqtt.crt_file= os.path.join(secrets_path,mqtt_config.crtfile)
    broker_mqtt.key_file = os.path.join(secrets_path,mqtt_config.keyfile)
    logging.debug("CA :%s"%broker_mqtt.ca_file)
    logging.debug("CRT:%s"%broker_mqtt.crt_file)
    logging.debug("KEY:%s"%broker_mqtt.key_file)
    broker_mqtt.start()

    watchdog_index = 0
    while(True):
        if(is_mqtt_connected):
            watchdog_index = 0
            pubret = broker_mqtt.publish(mqtt_config.topic_service,"heartbeat",qos=0)
            t = time.strftime("%a, %d %b %Y %H:%M:%S")
            logging.debug(f"HeartBeat : {t} : waiting for new detection")
            process_one_alprd_detection()
            #TODO: more complete watchdog (currently only wait first connection)
        else:
            watchdog_index += 1
            if (watchdog_index > watchdog_max_failed_attempt):
                logging.info(f"Watchdog : exiting the process")
                exit(1)
            t = time.strftime("%a, %d %b %Y %H:%M:%S")
            logging.info(f"HeartBeat : {t} : waiting for MQTT connexion")
            time.sleep(10)
         
