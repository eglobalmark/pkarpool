from jtop import jtop
import time
import paho.mqtt.client as mqtt
import os

def uptime():
 
     try:
         f = open( "/proc/uptime" )
         contents = f.read().split()
         f.close()
     except:
        return "Cannot open uptime file: /proc/uptime"
 
     total_seconds = float(contents[0])
 
     # Helper vars:
     MINUTE  = 60
     HOUR    = MINUTE * 60
     DAY     = HOUR * 24
 
     # Get the days, hours, etc:
     hours   = round(float( ( total_seconds % DAY ) / HOUR ),2)
     
     return hours
     
     
if __name__ == "__main__":
    client_name = "jano2"
    basename = "urn:dev:smartcam:edgegw_02_21_002"
    host_name = "localhost"
    
    print("Jetson Monitoring started")
	
    client = mqtt.Client(client_name)
    client.connect(host_name)
    with jtop() as jetson:
        # jetson.ok() will provide the proper update frequency
        if jetson.ok():
            senml_msg = [{"bn":basename,"bt":time.time()},
                         {"n":"uptime","u":"HUR","v":uptime()},
                         {"n":"power","u":"W","v":jetson.power[0]["cur"]/1000},
                         {"n":"gpu_power","u":"W","v":jetson.power[1]["5V GPU"]["cur"]/1000},
                         {"n":"cpu_power","u":"W","v":jetson.power[1]["5V CPU"]["cur"]/1000},
                         {"n":"cpu_temp","u":"C","v":jetson.temperature["CPU"]},
                         {"n":"gpu_temp","u":"C","v":jetson.temperature["GPU"]},
                         {"n":"disk_usage","u":"%","v":round((jetson.disk["available"]/jetson.disk["total"])*100,2)},
                         {"n":"ram_usage","u":"%","v":round((jetson.ram["use"]/jetson.ram["tot"])*100,2)}] 
            
            client.publish("edgeGW/",str(senml_msg).replace("'",'"'))
            
