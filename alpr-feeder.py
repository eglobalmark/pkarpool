import os, time
import json
import logging
import psutil, shutil
import requests
import greenstalk

DIR = "/home/egm/egm_hik_01/cambuffer/"
CACHE_DIR = "/home/egm/cache"
BS_HOST = "127.0.0.1"
BS_PORT = 11300
BS_TUBE = "alpr"

# Setting up logging
log_handler = logging.StreamHandler()
log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_handler.setFormatter(log_formatter)
root_logger = logging.getLogger()
root_logger.addHandler(log_handler)
root_logger.setLevel(logging.DEBUG)
log = logging.getLogger(__name__)
log.info("Logging setup done")


def get_ftp_files():
    files = []
    for p in psutil.process_iter():
        if (p.name() == 'vsftpd'):
            files.extend([i.path for i in p.open_files()])
    return files

def get_files_to_process():
    ready_files = []
    ftp_open_files = get_ftp_files()
    for f in os.listdir(DIR):
        f = os.path.join(DIR,f)
        isopen = f in ftp_open_files
        if (not isopen):
            ready_files.append(f)
    ready_files.sort()
    return ready_files

def get_alpr_results(image_path):
    regions = ['fr',]
    config = {}
    data = dict(regions=regions, config=json.dumps(config))
    with open(image_path,'rb') as fp:
        response = requests.post("http://localhost:8080/v1/plate-reader/",
                      files = dict(upload=fp),
                      data =data
                      )
        if (response.status_code < 200 or response.status_code > 300):
            log.error(f"request error {response.status_code} : {response.text}")
            return None
        else :
            log.info(f"reesponse {response.text}")
            return response.text

def finalize_file_processing(image_path):
    os.remove(image_path)
    #shutil.move(image_path, CACHE_DIR)
        
def main():
    bs = greenstalk.Client(host=BS_HOST, port=BS_PORT, use=BS_TUBE)
    while(True):
        time.sleep(1.0)
        files = get_files_to_process()
        for f in files:
            log.info(f"START : {f}")
            try:
                results = get_alpr_results(f)
                if (results) :
                    bs.put(results)
            finally:
                finalize_file_processing(f)
                log.info(f"END : {f}")
    
if __name__ == '__main__':
#    print("\n".join(get_ftp_files()))
    log.info("Starting ALPR Feeder")
    time.sleep(1.0)
    main()
    
