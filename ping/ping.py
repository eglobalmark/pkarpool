#!/usr/bin/python3 -u

import time


while(True):
    t = time.strftime("%a, %d %b %Y %H:%M:%S")
    print(f"PING:{t}")
    time.sleep(10)
