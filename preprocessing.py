import json
from openalpr import Alpr

alpr = Alpr("eu", "/home/egm/openalpr/openalpr.config", "/home/egm/openalpr/runtime_data")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)
results = alpr.recognize_file("/home/egm/openalpr/2plates.jpg")
print(json.dumps(results, indent=4))
alpr.unload()